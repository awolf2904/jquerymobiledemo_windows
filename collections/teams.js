Teams = new Meteor.Collection('teams');

Teams.allow({
	/*insert: function(){
		return false; //only inserts with addUser/addTeam allowed
	},*/
	update: function(userId, doc, fieldNames, modifier){
		//return true;
		//console.log(userId, 'userId update allow');
		//console.log(doc.ownerId, 'ownerId update allow');
		
		if (userId !== doc.ownerId)
      return false; // not the owner

    var allowed = ["teamName", "teamNameUpper", "teamMembers", "isFavourite"];
 
    if (_.difference(fieldNames, allowed).length)
      return false; // tried to write to forbidden field
    else
			return true;
	}, //ownsDocument,
	remove: ownsDocument
});

Meteor.methods({
	//returns team members for a given post
	getTeamInfo: function(post) {
		var teams = Teams.findOne({ownerId: post.userId, teamNameUpper: post.visibility.toUpperCase()}); //mapping not required was require because of sub in teams
		var teamMembers = "";
		if ( teams ) {
			teamMembers = _.map(teams.teamMembers, function(value) {
					console.log(value);
					return displayName(Meteor.users.findOne(value));
				});
		}
		return teamMembers;
	},

	generateTeamList: function() {
		var user = Meteor.user(); //s.find({userId: user._id}); //get current user
		//console.log("generateMembersList");
		//console.log(Meteor.users.find());

		if ( !user ) return ""; //return with-out error message just create no list

			//throw new Meteor.Error(401, "You need to login to add a team member!");

		/*var teams = _.map(Teams.find({userId: user._id}).fetch(),
													function(value){
														return {
															teamId: value._id,
															teamName: value.teamName
													};
											});*/

		var teams;
		//team manager functionailty
		teams = Teams.find({ownerId: user._id}).fetch(); //mapping not required was require because of sub in teams
		teams = _.sortBy(teams, function(value) {
			return !value.isFavourite;
		});
		teams = _.map(teams, function(list) {
														return _.omit(list, ['ownerId','teamMembers']); //remove userIds from session variable
											});

   console.log(teams, "teams sorted");
		//var teamIds = = _.map(Teams.find({userId: user._id}).fetch(), function(value){ return value._id});
		//console.log("generateTeamList method:");
		//console.log(teams);
		var out;
		//console.log(user);
		
		if (_.isUndefined(teams)) {
			out = ["No teams available!!"];
		}
		else
		{
			out = teams;
			//out.ids = teamIds;
		}

		//console.log(out);
		return out;
	},
	addUser: function(newUser, team, options)
	{	// team = {teamId,teamName}
		//var checkResult = false;
		var teamId;
		var isTeamOnly = false;
		var user = Meteor.user(); //get current user
		var teamExists = Teams.findOne({ownerId: user._id, teamNameUpper: String(team.teamName).toUpperCase()});
		console.log(teamExists, "teamExists");
		console.log("addUser Method:");
		//console.log(user);
		
		if ( !user )
			throw new Meteor.Error(401, "You need to login to add a team member!");
		
		console.log(options);

		if ( options && options.insertType == 'teamOnly') {
			isTeamOnly = true;
		}
		else
		{
			if ( !newUser )
				throw new Meteor.Error(422, 'Please fill in a username or e-mail address!');
		}

		if ( !team.teamName )
			throw new Meteor.Error(422, 'Please fill in a team name!');
		
		// check that the team name is unique in DB

		if ( teamExists && isTeamOnly ) //check if it is team insert
			throw new Meteor.Error(302, 'This team name already exists',
				teamExists._id);


		var curUserId;
		//console.log("Email: ");
		//console.log(Meteor.users.findOne({'emails.address': {$regex:user,$options:'i'}}));
		if ( ! _.isUndefined(Meteor.users.findOne({'emails.address': {$regex:newUser,$options:'i'}})) ){
			curUserId = Meteor.users.findOne({'emails.address': {$regex:newUser,$options:'i'}})._id;
		}
		else {
			var userInDB = Meteor.users.findOne({username: newUser});
			if ( userInDB )
			{
				curUserId = userInDB._id; //Meteor.users.find({username: newUser});
			}
		}
		console.log("user id to add" + curUserId);
		console.log("logged in userId = " + user._id);
		console.log("team name= " + team.teamName);
		if ( ! _.isUndefined(curUserId) || isTeamOnly ) { //ignore not specified user if isTeamOnly is true
			console.log('team count ' + Teams.find({ownerId: user._id, teamName: String(team.teamName)}).count());
			if ( Teams.find({ownerId: user._id, teamNameUpper: String(team.teamName).toUpperCase()}).count() === 0 ) {
				//team not available in DB
				var newTeam = {
					ownerId: user._id,
					//team: { //no need to do a sub section
					teamName: String(team.teamName),
					teamNameUpper: String(team.teamName).toUpperCase(),
					createDate: new Date().getTime(),
					teamMembers: [],
					isFavourite: false
						//}
					};

				if ( ! isTeamOnly ) {
					newTeam.teamMembers.push(curUserId);
					newTeam.teamMembers = _.uniq(newTeam.teamMembers); //remove duplicates if any
				}

				//if ( Teams.find(newTeam).count() == 0 )	{
				teamId = Teams.insert(newTeam); //only inset if it is not there
        //Session.set('selectedTeam', teamId);
				console.log("teamId new team insert:");
				console.log(teamId);
				console.log(newTeam.teamNameUpper);
				
				//console.log(Teams.findOne(teamId));
				// regenerate team list
				/*Meteor.call('generateTeamList', function(error,result) {
					if (!error) {
						Session.set("curTeamList",result);
					}
				});*/

				return {_id:teamId, teamName: newTeam.teamName};
				/*Teams.update({userId: user._id}, 
					{
						$addToSet: {
							'team.teamMembers': [
									curUserId ]
							}
						}
					);*/
					//, addedDate: new Date().getTime()}})
			}
			else
			{ // team name available in teams
				if ( ! isTeamOnly ) {
					//updating only with users
					console.log('adding ' + curUserId);
					console.log('teamname = ' + team.teamName);
					teamId = Teams.update({ownerId: user._id, _id: team._id}, //'team.teamName': String(team.teamName)}, 
					{
						$addToSet: {
							teamMembers: curUserId //, 
								//addedDate: new Date().getTime()} //added date later --> check for existence problem
						}
					},
					//{upsert:true}, 
					function(error){
						if ( error ) {
							console.log(error);
							//throwError(error);
						}
						else {
							//console.log("modified " + recordCount);
							//no error
						}
					}); //only add if not already in list --> addToSet checks that
				//}	//upsert feature not implemented in Meteor --> throws an error!!!!!!!!!!!!
					//Session.set('selectedTeam', teamId);
					console.log('teamId -->' + teamId);
					return {_id:team._id, teamName: team.teamName};
				}
			/*else {
				return false;	//user not in db --> show user send e-mail request for invite
			}*/
				else
				{ //team exits return message on isOnly adding
					throw new Meteor.Error(422, 'Team name: ' + team.teamName + ' already exits!');
					//select that team.
					//Session.set('selectedTeam', teamId);
					// return {teamId:team.teamId, teamName: team.teamName}; //<------------unreachable after throw
				}
			}
		}
	else {
		throw new Meteor.Error(422, 'User that you want to add is not registered!'); //later send e-mail invite
	}
 },
 addTeam: function(team, options){
		return Meteor.call('addUser', null, team, options, function (error, result) {
			if ( ! error ) {
				console.log("result from addUser in addTeam: ");
				console.log(result);
				return result;
			}
		});
	}
});

///////////////////////////////////////////////////////////////////////////////
// Users

displayName = function (user) {
  //if (user.profile && user.profile.name)
  //  return user.profile.name;
  if ( user ) return user.username;
  /*else
		return user.emails[0].address;*/
};

var contactEmail = function (user) {
  if (user.emails && user.emails.length)
    return user.emails[0].address;
  if (user.services && user.services.facebook && user.services.facebook.email)
    return user.services.facebook.email;
  return null;
};
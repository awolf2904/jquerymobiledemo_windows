Router.map(function () {
  this.route('home', {
		path: '/',
		//controller: 'HomeController',
		}, function() {
			$.mobile.changePage( "#demo-page" , { reverse: false, changeHash: false } );
		});

  this.route('items', {
    controller: 'ItemsController',
    action: 'customAction'
  });
});

// HomeController = RouteController.extend({
// template: 'home',
// show: function(){
// $.mobile.changePage( "#demo-page" , { reverse: false, changeHash: false } );
//}
// });


Router.configure({
    layout: 'layout',
    notFoundTemplate: 'notFound',
    loadingTemplate: 'loading'
  });

  Subscriptions = {
    posts: Meteor.subscribe('posts')
  };

  ItemsController = RouteController.extend({
    template: 'items',

    /*
     * During rendering, wait on the items subscription and show the loading
     * template while the subscription is not ready. This can also be a function
     * that returns on subscription handle or an array of subscription handles.
     */

    waitOn: Subscriptions['posts'],

    /*
     * The data function will be called after the subscrition is ready, at
     * render time.
     */

    data: function () {
      // we can return anything here, but since I don't want to use 'this' in
      // as the each parameter, I'm just returning an object here with a named
      // property.
      return {
        items: Posts.find()
      };
    },

    /*
     * By default the router will call the *run* method which will render the
     * controller's template (or the template with the same name as the route)
     * to the main yield area {{yield}}. But you can provide your own action
     * methods as well.
     */
    customAction: function () {

      /* render customController into the main yield */
      // Programatically changes to the categories page
      $.mobile.changePage( "#demo-page" , { reverse: false, changeHash: false } );

      this.render('items');

      /*
       * You can call render multiple times. You can even pass an object of
       * template names (keys) to render options (values). Typically, the
       * options object would include a *to* property specifiying the named
       * yield to render to.
       *
       */
      this.render({
        itemsAside: { to: 'aside', waitOn: false, data: false },
        itemsFooter: { to: 'footer', waitOn: false, data: false }
      });
    }
  });
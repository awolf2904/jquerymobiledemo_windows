// check that the userId specified owns the documents
ownsDocument = function(userID, doc) {
	//console.log(doc.userId);
	//console.log(userId);
  return doc && doc.userId === userID;
}